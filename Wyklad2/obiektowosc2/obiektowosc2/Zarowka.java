package obiektowosc2;

public class Zarowka {
	public static int MINIMALNA_JASNOSC = 1;
	private static int MAKSYMALAN_JASNOSC = 10;
	
	public static int dajMinimalnaJasnosc() {
		return MINIMALNA_JASNOSC;
	}
	
	static {
		MINIMALNA_JASNOSC = 2;
	}
	
	// domyslan wartosc = false
	private boolean swieci;

	// poziom jasonsci od 1 do 10
	private int poziomJasnosci = MAKSYMALAN_JASNOSC;

	public void zapal() {
		this.swieci = true;
	}

	public void zgas() {
		swieci = false;
	}
	
	public void setSwieci(boolean noweswieci) {
		Zarowka.this.swieci = noweswieci;
	}

	public boolean czySwieci() {
		return swieci;
	}

	public int poziomJasnosci() {
		return poziomJasnosci;
	}
}
