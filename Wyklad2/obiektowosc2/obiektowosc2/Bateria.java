package obiektowosc2;

public class Bateria {
	// przyjmuje ze moc baterii w % od 0 do 100
	private int moc;

	{
		this.moc = 100;
	}

	{
		this.moc = 102;
	}

	public boolean czyMaMoc() {
		return moc > 0;
	}

	public void zmienjszMocO(int wartosc) {
		moc -= wartosc;
	}
}
