package obiektowosc2;

public class Latarka {
	private Zarowka zarowka;
	private Bateria[] baterie = new Bateria[4];

	public void wlacz() {
		// wlacz zarowke jezeli istnieje i jezeli jest wystarczajaco mocy w
		// bateriach
		if (zarowka != null && czyWystarczyMocy()) {
			zarowka.zapal();
			zmniejszMocBaterii(zarowka.poziomJasnosci());
		}
	}
	
	public boolean czySwieci() {
		if(zarowka != null) {
			return zarowka.czySwieci();
		}
		return false;
	}
	
	public boolean czyMogeWlaczyc() {
		return zarowka != null && czyWystarczyMocy();
	}

	public void wylacz() {
		if (zarowka != null) {
			zarowka.zgas();
		}
	}

	public void wymienZarowke(Zarowka nowaZarowka) {
		// najpierw sprawdz czy zarowka się swieci (nie chce się oparzyc)
		if (zarowka != null && zarowka.czySwieci()) {
			zarowka.zgas();
		}
		this.zarowka = nowaZarowka;
	}

	public void wymienBaterie(Bateria... noweBaterie) {
		this.baterie = noweBaterie;
	}

	private boolean czyWystarczyMocy() {
		// maksymalna liczba baterii 4
		boolean czyJestMoc = false;

		// 1. musza byc 4 baterie
		if (baterie != null && baterie.length == 4) {

			for (Bateria bateria : baterie) {
				if (bateria == null) {
					// nie ma baterii nie ma mocy
					return false;
				}
				// 2. przynajmniej jedna bateria ma moc
				if (bateria.czyMaMoc()) {
					czyJestMoc = true;
				}
			}
		}

		return czyJestMoc;
	}

	private void zmniejszMocBaterii(int poziomJasnosciZarowki) {
		for (Bateria bateria : baterie) {
			bateria.zmienjszMocO(poziomJasnosciZarowki);
		}
	}

}
