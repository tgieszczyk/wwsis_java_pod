import obiektowosc2.Bateria;
import obiektowosc2.Latarka;
import obiektowosc2.Zarowka;

public class LatarkaTest {

	public static void main(String[] args) {
		System.out.println(Zarowka.MINIMALNA_JASNOSC);
		
		Latarka latarka = new Latarka();
		Zarowka zarowka = new Zarowka();
		latarka.wymienZarowke(zarowka);
		Bateria[] baterie = new Bateria[] { new Bateria(), 
				new Bateria(),
				new Bateria(), 
				new Bateria() };
		latarka.wymienBaterie(new Bateria(), 
				new Bateria(),
				new Bateria(), 
				new Bateria() );
		latarka.wymienBaterie(baterie);
		
		while(latarka.czyMogeWlaczyc()) {
			latarka.wlacz();
			
			System.out.println("latarka swieci");
			
			latarka.wylacz();
		}
		
		
	}

}
