public class Zarowka {
	private boolean zapalona;
	/* skala od 1 do 10 (10 najja�niejsza) */
	private int jasnosc = 10;

	public void zga�() {
		zapalona = false;
	}

	public void zapal() {
		zapalona = true;
	}

	public void przyciemnij() {
		jasnosc--;
		if (jasnosc <= 0) {
			zga�();
			jasnosc = 0;
		}
	}

	public void rozja�nij() {
		if (jasnosc < 10) {
			jasnosc++;
		}
		if (!zapalona) {
			zapal();
		}
	}
}
