public class Latarka {
	private boolean wlaczona;
	private Zarowka zarowka;

	public void wlacz() {
		wlaczona = true;
		if (zarowka != null) {
			zarowka.zapal();
		}
	}

	public void wylacz() {
		wlaczona = false;
		if (zarowka != null) {
			zarowka.zga�();
		}
	}

	public void wkrecZarowke() {
		zarowka = new Zarowka();
	}

	public void wkrecZarowke(Integer v) {

	}

	public void wkrecZarowke(Object v, boolean czyFaktycznieWkrecic) {

	}

	public void wkrecZarowke(boolean czyFaktycznieWkrecic, Object v) {

	}

	public void wkrecZarowke(boolean czyFaktycznieWkrecic, Object... v) {
		for(Object v1 : v) {
			
		}
		for( int i = 0; i < v.length; i++ ) {
			
		}
	}
	
	public void wywolaj(Object v) {
		
	}

	public void wykrecZarowke() {
		Integer v = null;
		
		
		wywolaj(v);
		wkrecZarowke(true);
		zarowka = null;
	}

	public void setWloaczona(boolean wlaczona) {
		this.wlaczona = wlaczona;
	}

	public boolean isWlaczona() {
		return this.wlaczona;
	}
}
