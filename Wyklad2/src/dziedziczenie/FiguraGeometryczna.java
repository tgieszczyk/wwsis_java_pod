package dziedziczenie;

public interface FiguraGeometryczna {
	double obliczPole();
	double obliczObwod();
	String nazwa();
}
