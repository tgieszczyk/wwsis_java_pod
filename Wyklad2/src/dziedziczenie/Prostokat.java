package dziedziczenie;

public class Prostokat implements FiguraGeometryczna {
	private double dlugosc;
	private double szerokosc;

	public Prostokat(double dlugosc, double szerokosc) {
		this.dlugosc = dlugosc;
		this.szerokosc = szerokosc;
	}
	
	@Override
	public double obliczObwod() {
		return (2 * dlugosc) + (2 * szerokosc);
	}

	@Override
	public double obliczPole() {
		return dlugosc * szerokosc;
	}
	
	@Override
	public String nazwa() {
		return "Prostokat";
	}
}
