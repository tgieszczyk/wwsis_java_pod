package dziedziczenie;

public class FiguraGeometrycznaTest {

	public static void main(String[] args) {
		FiguraGeometryczna p = new Prostokat(10, 8);
		// kastowanie na typ obiektu
		printInfo((Prostokat) p);
		Kwadrat k = new Kwadrat(20);
		printInfo(k);

	}

	public static void printInfo(FiguraGeometryczna fg) {
		if (fg instanceof Kwadrat) {
			printInfoOKwadracie((Kwadrat) fg);
		} else {
			System.out.println("Obwod " + fg.nazwa() + ": " + fg.obliczObwod());
			System.out.println("Pole " + fg.nazwa() + ": " + fg.obliczPole());
		}

	}

	public static void printInfoOKwadracie(Kwadrat fg) {
		System.out.println("Obwod kwadratu "+ fg.nazwa() + ": " + fg.obliczObwod());
		System.out.println("Pole kwadratu "+ fg.nazwa() + ": " + fg.obliczPole());
	}

}
