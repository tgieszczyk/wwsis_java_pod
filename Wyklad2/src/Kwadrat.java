public class Kwadrat {
	private int dlugoscBoku;
	private String opis = "domyslny";
	private String zmienna;

	{

		System.out.println("blok inicjalizacyjny 1");
	}

	public Kwadrat() {

		System.out.println("konstruktor domyslny");
	}

	{

		System.out.println("blok inicjalizacyjny 4");
	}

	public Kwadrat(int dlugoscBoku) {		
		this.dlugoscBoku = dlugoscBoku;
		System.out.println("konstruktor dlugoscboku");

	}

	{

		System.out.println("blok inicjalizacyjny 2");
	}

	public Kwadrat(String opis) {
		this.opis = opis;
		System.out.println("konstruktor opis");
	}

	{

		System.out.println("blok inicjalizacyjny 3");
	}

	public int obliczPole() {
		return liczToPole(dlugoscBoku);
	}
	
	
	private int liczToPole(int dlugoscBoku)  {
		return dlugoscBoku * dlugoscBoku;
	}
}
