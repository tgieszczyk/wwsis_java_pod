package przyklady.zjazd2;

public class Wartosc {
	private String wartosc;
	private final String domyslnaWartosc ;

	{
		this.domyslnaWartosc = "<nowa domyslna>";
	}

	public Wartosc() {
		this("Domyslny");
//		System.out.println("Konstruktor");
	}

	public Wartosc(String wartosc) {
		this(wartosc, "");
		this.wartosc = wartosc;
	}

	public Wartosc(String wartosc, String wartosc2) {
		this.wartosc = wartosc;
	}

	public String getWartosc() {
		return wartosc;
	}

	public void setWartosc(String wartosc) {
		this.wartosc = wartosc;
	}

	@Override
	public String toString() {
		return wartosc;
	}

//	{
//		this.wartosc = "jakis domyslny";
//		System.out.println("Blok konstrukcyjny1");
//	}

//	{
//		this.wartosc = "jakis domyslny";
//		System.out.println("Blok konstrukcyjny2");
//	}
	
	class WartoscWewnetrzan {
		
	}
}



