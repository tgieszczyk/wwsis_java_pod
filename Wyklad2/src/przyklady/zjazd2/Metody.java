package przyklady.zjazd2;

import przyklady.zjazd2.Wartosc.WartoscWewnetrzan;

public class Metody {
	public void mojaMetoda(Wartosc wartosc) {
		System.out.println(wartosc.getWartosc());
		wartosc.setWartosc("Nowy Tomasz");
		wartosc = new Wartosc();
		System.out.println(wartosc.getWartosc());
	}
	
	public static void main(String[] args) {
		Metody metody = new Metody();
		
		
		final Wartosc w;
		w = new Wartosc();
		
//		w.setWartosc("Tomasz");
//		
//		WartoscWewnetrzan ww = w.new WartoscWewnetrzan();
//		

		class MojaKlasa {
			
		}
		
		MojaKlasa mk = new MojaKlasa();
		
		metody.mojaMetoda(new Wartosc(){
			@Override
			public String getWartosc() {
				return "moja nowa wartosc";
			}
		});
		
//		System.out.println("Po wywolaniu metody : " + w.getWartosc());
		
		
		
		
	}
}
