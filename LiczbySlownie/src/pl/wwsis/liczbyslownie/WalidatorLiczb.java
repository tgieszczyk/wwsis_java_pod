package pl.wwsis.liczbyslownie;

/**
 * Klasa sluzy do sprawdzenia czy ciag znakow jest liczba
 * 
 * @author a051213
 *
 */
public class WalidatorLiczb {

	private final static char[] CYFRY = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	/**
	 * Sprawdzam czy ciag znakow jest faktycznie liczba
	 * 
	 * @param wartosc
	 * @return
	 */
	public boolean czyPoprawnaLiczba(String wartosc) {
		for (int i = 0; i < wartosc.length(); i++) {
			char kandydatNaLiczbe = wartosc.charAt(i);
			// sprawdzamy czy kandydat jest liczba
			try {
				Integer.parseInt(String.valueOf(kandydatNaLiczbe));
			} catch (NumberFormatException e) {
				// wartosc nie jest liczba
				e.printStackTrace();
				return false;
			}

			// mozna tak ale wydajnosciowo nie jest to najlepsze rozwiazanie
			// for(char cyfra : CYFRY) {
			// if(cyfra != kandydatNaLiczbe) {
			// return false;
			// }
			// }
			// if(kandydatNaLiczbe)
		}

		return true;

	}

	// testujemy walidator
	public static void main(String[] args) {
		WalidatorLiczb wl = new WalidatorLiczb();

		boolean liczbaPoprawna = wl.czyPoprawnaLiczba("12349870");
		System.out.println("Oczekiwane true: " + liczbaPoprawna);

		boolean liczbaNiepoprawna = wl.czyPoprawnaLiczba("12349870w");
		System.out.println("Oczekiwane false: " + liczbaNiepoprawna);
	}

}
