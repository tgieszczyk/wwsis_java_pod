package pl.wwsis.liczbyslownie;

public class LiczbySlownieMain {

	private static final int ZWYKLE_LICZBY = -1; // dlugosc znakow maksymalnie 3
	private static final int TYSIACE = 0; // dlugosc znakow pomiedzy 4 a 6
	private static final int MILIONY = 1;// dlugosc znakow pomiedzy 7 a 9
	private static final int MILIARDY = 2;// dlugosc znakow pomiedzy 10 a 12

	private static final String[] JEDNOSTKI = { "zero", "jeden", "dwa", "trzy", "cztery", "piec", "szesc", "siedem",
			"osiem", "dziewiec" };
	private static final String[] NASTRKI = { "jedenascie", "dwanascie", "trzynascie", "czternascie", "pietnascie",
			"szesnascie", "siedemnascie", "osiemnascie", "dziewietnascie" };
	private static final String[] DZIESIATKI = { "dziesiec", "dwadziescia", "tzydziesci", "czterdziesci",
			"piecdziesiat", "szescdziesiat", "siedemdziesiat", "osiemdziesiat", "dziewiecdziesiat" };
	private static final String[] SETKI = { "sto", "dwiescie", "trzysta", "czterysta", "piecset", "szescset",
			"siedemset", "osiemset", "dziewiecset" };

	public static void main(String[] args) {
		// obsluga bledow
		if (args.length != 1) {
			System.out.println("Nie podano parametru (podaj liczbe jako parametr programu)");
			return;
		}

		String parametr = args[0];

		// obsluga bledow ciag dalszy
		WalidatorLiczb walidatorLiczb = new WalidatorLiczb();
		if (!walidatorLiczb.czyPoprawnaLiczba(parametr)) {
			System.out.println("Podany przez Ciebie parametr nie jest liczba -> podaj poprawna liczbe");
			return;
		}

		int typLiczby = rozpoznajTypLiczby(parametr);
		if (typLiczby == -2) {
			System.out.println("Za dluga liczba, podaj liczbae w maksymalnie miliardach");
			return;
		}

		// liczba jest poprawna
		System.out.println(parametr);

		String wynik = konwertujDoSlownie(parametr, typLiczby);
		System.out.println(wynik);

	}

	public static String konwertujDoSlownie(String liczba, int typLiczby) {
		if (typLiczby != ZWYKLE_LICZBY) {
			String liczbaPrzed = pobierzWycietaLiczbePrzed(liczba, typLiczby);
			String liczbaPo = pobierzWycietejLiczbyPo(liczba, typLiczby);

			String odpowiedniaOdmiana = pobierzOdpowiedniaOdmiane(typLiczby, Integer.parseInt(liczbaPrzed));
			String resultatZOdmiana = konwertujDoSlownie(liczbaPrzed, ZWYKLE_LICZBY) + odpowiedniaOdmiana;

			int typLiczbyPo = rozpoznajTypLiczby(liczbaPo);

			String resultatLiczbyPo = konwertujDoSlownie(liczbaPo, typLiczbyPo);

			// 1 234 586 - typ liczby miliony
			// liczbaPrzed -> 1
			// liczbaPo -> 234 586

			return resultatZOdmiana + " " + resultatLiczbyPo;
		} else {

			if (liczba.length() == 1) {
				// jest to jednostka
				int wartosc = Integer.parseInt(liczba);
				return JEDNOSTKI[wartosc];
			}

			if (liczba.length() == 2) {
				// obsluga liczb z zakresu od 10 do 99
				int wartosc = Integer.parseInt(liczba);
				if (wartosc != 0) {
					// sprawdzam czy sa nastki
					if (czyLiczbaJestNastka(wartosc)) {
						// tablica nastki zaczyna sie od indeksu 0
						// w celu uzystkania prawidlowego indexu nalezy wartosc
						// odjac 11
						int indexNastek = wartosc - 11;
						return NASTRKI[indexNastek];
					}

					// dziesiatki
					//
					int indexDiesiatek = (wartosc / 10) - 1;

					String dziesiatkiSlownie = DZIESIATKI[indexDiesiatek];
					String jednostkiSlownie = "";
					// przy dziesiatka nie piszemy zero
					if (wartosc % 10 != 0) {
						jednostkiSlownie = konwertujDoSlownie((wartosc % 10) + "", ZWYKLE_LICZBY);
					}

					return dziesiatkiSlownie + " " + jednostkiSlownie;
				}
			}

			if (liczba.length() == 3) {
				// obsluga setek
				int wartosc = Integer.parseInt(liczba);
				if (wartosc != 0) {
					int indexSetek = (wartosc / 100) - 1;
					String setkiSlownie = SETKI[indexSetek];
					String dziesiatkiSlownie = "";
					if (wartosc % 100 != 0) {
						dziesiatkiSlownie = konwertujDoSlownie((wartosc % 100) + "", ZWYKLE_LICZBY);
					}
					return setkiSlownie + " " + dziesiatkiSlownie;
				}
			}
		}

		return "";
	}

	private static int rozpoznajTypLiczby(String liczba) {
		switch (liczba.length()) {
		case 1:
		case 2:
		case 3:
			return ZWYKLE_LICZBY;
		case 4:
		case 5:
		case 6:
			return TYSIACE;
		case 7:
		case 8:
		case 9:
			return MILIONY;
		case 10:
		case 11:
		case 12:
			return MILIARDY;
		}
		return -2;
	}

	private static final String[][] LICZBY_WIELKIE = { { "tysiac", "tysiace", "tysiecy" },
			{ "milion", "miliony", "milionow" }, { "miliard", "miliardy", "miliardow" } };

	private static String pobierzOdpowiedniaOdmiane(int typLiczby, int wartoscWSetkach) {
		if (wartoscWSetkach == 0) {
			return "";
		}
		switch (typLiczby) {
		case ZWYKLE_LICZBY:
			return "";
		case TYSIACE:
		case MILIONY:
		case MILIARDY: {
			int indexOdmiany = 2;
			if (wartoscWSetkach == 1) {
				indexOdmiany = 0;
			} else if (czyIndexOdmianyJeden(wartoscWSetkach + "")) {
				indexOdmiany = 1;
			}
			return " " + LICZBY_WIELKIE[typLiczby][indexOdmiany];
		}
		}

		return " <nieobslugiwane>";
	}

	private static boolean czyLiczbaJestNastka(int liczba) {
		return liczba >= 11 && liczba <= 19;
	}

	/**
	 * indeks odmiany jeden to 'tysiace', 'miliony' lub 'miliardy'
	 * 
	 * @param liczbaStr
	 * @return
	 */
	private static boolean czyIndexOdmianyJeden(String liczbaStr) {
		if (liczbaStr.length() > 1) {
			int indexDziesitek = liczbaStr.length() - 2;
			// czy sa nastki
			if (liczbaStr.charAt(indexDziesitek) == '1') {
				return false;
			}
		}

		int indexJednostek = liczbaStr.length() - 1;
		char jednostka = liczbaStr.charAt(indexJednostek);
		if (jednostka == '2' || jednostka == '3' || jednostka == '4') {
			return true;
		}

		return false;
	}

	/**
	 * przyklad 1 234 586: wycinam w przypadku milionow 1
	 * 
	 * @param liczba
	 * @param typLiczby
	 * @return
	 */
	private static String pobierzWycietaLiczbePrzed(String liczba, int typLiczby) {
		switch (typLiczby) {
		case TYSIACE:
			return liczba.substring(0, liczba.length() - 3);
		case MILIONY:
			return liczba.substring(0, liczba.length() - 6);
		case MILIARDY:
			return liczba.substring(0, liczba.length() - 9);
		}
		return liczba;
	}

	/**
	 * przyklad 1 234 586: wycinam w przypadku milionow 234 586
	 * 
	 * @param liczba
	 * @param typLiczby
	 * @return
	 */
	private static String pobierzWycietejLiczbyPo(String liczba, int typLiczby) {
		switch (typLiczby) {
		case TYSIACE:
			return liczba.substring(liczba.length() - 3);
		case MILIONY:
			return liczba.substring(liczba.length() - 6);
		case MILIARDY:
			return liczba.substring(liczba.length() - 9);
		}
		return liczba;
	}
}
