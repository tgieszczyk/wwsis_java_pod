package pl.wwsis.liczbyslownie;

public enum TypLiczby {
	ZWYKLA_LICZBA(1, 3), TYSIACE(4, 6, "tysiac", "tysiace", "tysiecy"), MILIONY(7, 9), MILIARDY(10, 12), NIEROZPOZNANY;
	
	private String[] odmiany;
	private int minDlugosc;
	private int maxDlugosc;
	
	private TypLiczby() {
		// tego trzeba sie pozbyc ?
		// TODO Auto-generated constructor stub
	}
	
	private TypLiczby(int minDlugosc, int maxDlugosc, String... odmiany) {
		this.odmiany = odmiany;
		this.minDlugosc = minDlugosc;
		this.maxDlugosc = maxDlugosc;
	}
	
	
	public String pobierzOdpowiedniaOdmiane(String liczba) {
		return "";
	}
	
	public static TypLiczby rozpoznajTypLiczby(String liczba) {
		for (TypLiczby typLiczby : values()) {
			if(liczba.length() >= typLiczby.minDlugosc && liczba.length() <= typLiczby.maxDlugosc) {
				return typLiczby;
			}
		}
		return NIEROZPOZNANY;
	}
}
