/**
 * Program tworzy tablic� tr�jk�tn� int[][], wype�nia j� losowymi liczbami<br/>
 * ca�kowitymi i wypisuje na standardowe wyj�cie.<br/>
 * Istotne elementy w programie:<br/>
 * <ul>
 * <li>utworzenie tablicy tr�jk�tnej;
 * <li>losowanie liczby rzeczywistej z przedzia�u [0,1) metod� Math.random().
 * </ul>
 */
public class TablicaTrojkatna {
	public static void main(String[] args) {
		// deklaracja tablicy 2D
		int tab[][];
		// utworzenie tablicy g��wnej o losowej d�ugo�ci
		tab = new int[(int) (Math.random() * 10)][];
		// utworzenie podtablic r�nych rozmiar�w
		for (int i = 0; i < tab.length; i++)
			tab[i] = new int[i];
		// wype�nienie tablicy 2D losowymi warto�ciami
		for (int i = 0; i < tab.length; i++)
			for (int j = 0; j < tab[i].length; j++)
				tab[i][j] = (int) (Math.random() * 90 + 10);
		// wypisanie tablicy 2D
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab[i].length; j++)
				System.out.print(" " + tab[i][j]);
			System.out.println();
		}
	}
}
