/**
 * Program wypisuje na standardowym wyj�ciu wszystkie argumenty z jakimi zosta�
 * wywo�any.<br/>
 * Istotne elementy w programie:<br/>
 * <ul>
 * <li>wykorzystanie argumentu metody main(String[]);
 * <li>operacja konkatenacji �a�cuc�w znakowych z innymi typami danych;
 * <li>u�ycie pola length z tablicy napis�w.
 * </ul>
 * Przyk�ad<br/>
 * <code>java ArgumentyProgramu To s� moje pierwsze zaj�cia z Jav�</code> <br/>
 * Wynik:<br/>
 * <code>args[0] = To<br/>
args[1] = sa<br/>
args[2] = moje<br/>
args[3] = pierwsze<br/>
args[4] = zajecia<br/>
args[5] = z<br/>
args[6] = Java</code>
 */
public class ArgumentyProgramu {
	public static void main(String[] args) {
		for (int i = 0; i < args.length; i++)
			/*
			 * sk�adanie napisu za pomoc� konkatenacji
			 */
			System.out.println("args[" + i + "] = " + args[i]);

	}
}
