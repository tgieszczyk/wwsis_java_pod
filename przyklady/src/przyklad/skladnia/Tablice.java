package przyklad.skladnia;

public class Tablice {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// tablice jednowymiarowe
		int[] tablica1 = { 1, 2, 3 };
		int tablica2[] = new int[] { 1, 2, 3 };
		int tablica3[] = new int[3];
		int k =9;
		k%=2;

		for (int i = 0; i < tablica3.length; i++) {
			tablica3[i] = i + 1;
		}

		// tablice wielowymiarowe
		int[][] tablicaW1 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7 }, { 8, 9 } };
		int[][] tablicaw2 = new int[4][5];
		int[][][] tablicaW3 = new int[4][][];

		for (int i = 0; i < tablicaW3.length; i++) {
			tablicaW3[i] = new int[3][];
			for (int j = 0; j < tablicaW3[i].length; j++) {
				int[] finalnaTablica = { 1, 2, 3 };
				
				tablicaW3[i][j] = finalnaTablica;
			}
		}

	}

}
