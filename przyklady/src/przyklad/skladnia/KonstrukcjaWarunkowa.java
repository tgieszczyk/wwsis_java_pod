/**
 * 
 */
package przyklad.skladnia;

/**
 * @author tomek
 *
 */
public class KonstrukcjaWarunkowa {
	/* Zmienne globalne w obrebie danej klasy */
	float wynagrodzenie = 3500f;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		float progDobregoWynagrodzenia = 7500f;
		String zarabiamDuzo = "Tak zdecydowanie zarabiam du�o :D.";
		String zarabiamMalo = "Z cala pewnoscia powinienem / powinnam zarabiac wiecej!!!";
		KonstrukcjaWarunkowa mojeWarunki = new KonstrukcjaWarunkowa();
		// 1. 
		if(mojeWarunki.czyZarabiamDuzo(progDobregoWynagrodzenia)) {
			System.out.println(zarabiamDuzo);
		} else {
			System.out.println(zarabiamMalo);
		}
		
		
		// 2.
		String jakZarabiam = (mojeWarunki.czyZarabiamMalo(progDobregoWynagrodzenia)?zarabiamMalo:zarabiamDuzo);
		System.out.println(jakZarabiam);
		
	}
	
	
	boolean czyZarabiamDuzo(float progDobregoWynagrodzenia) {
		return wynagrodzenie >= progDobregoWynagrodzenia;
	}
	
	
	boolean czyZarabiamMalo(float progDobregoWynagrodzenia) {
		return !czyZarabiamDuzo(progDobregoWynagrodzenia);
	}

}
