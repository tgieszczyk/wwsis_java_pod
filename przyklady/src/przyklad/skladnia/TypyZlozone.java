/**
 * 
 */
package przyklad.skladnia;

/**
 * @author tomek
 *
 */
public class TypyZlozone {
	/* Zmienne globalne w danej klasie*/
	Integer licznik = 0;
	Float wynagrodzenie = 15000f;
	Character znak1 = 64;
	Character znak2= 'A';

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		TypyZlozone proste = new TypyZlozone();

		System.out.println("licznik: " + proste.licznik);
		System.out.println("wynagrodzenie: " + proste.wynagrodzenie);
		System.out.println("znak1: " + proste.znak1);
		System.out.println("znak2: " + proste.znak2);
		
	}

}
