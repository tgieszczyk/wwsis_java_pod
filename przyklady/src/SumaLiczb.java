/**
 * Program przekszta�ca wszystkie argumenty z jakimi zosta� wywo�any na liczby<br/>
 * ca�kowite, sumuje je i wynik wypisuje na standardowe wyj�cie.<br/>
 * Istotne elementy w programie:<br/>
 * <ul>
 * <li>wykorzystanie klasy opakowuj�cej Integer do wykonania konwersji.
 * </ul>
 */
public class SumaLiczb {
	public static void main(String[] args) throws Exception {
		double suma = 0, k =9;
		for (int i = 0; i < args.length; i++)
			/*
			 * obiekt Integer przechowuje liczb� typu int mo�na go zainicjowa�
			 * napisem zawieraj�cym liczb� ca�kowit�
			 */
			//suma += new Integer(args[i]);
			suma += new Double(args[i]);
		System.out.println(suma);
	}
}
